var express = require('express');
var app = express();
var srl = require('./srl.json')
var morgan = require('morgan')

app.use(morgan('dev'))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send(srl);
});

app.get('/:page/', function(req, res) {
  res.send(srl[req.params.page]);
});

app.get('/:page/:subject', function(req, res) {
  console.log(req.params.subject);
  res.send(srl[req.params.page][req.params.subject]);
});

app.get('/:page/:subject/:group', function(req, res) {
  res.send(srl[req.params.page][req.params.subject][req.params.group]);
});

app.listen(3002)
